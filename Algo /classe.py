### Importations
from typing import Dict, Tuple, Set, List

###Les fonctions
classe_d={
    'Quentin' : 174,
    'Ilyas' : 180,
    'Emma' : 163
}
classe_l = [
    ('Quentin', 174),
    ('Ilyas' , 180),
    ('Emma' , 163)
]
classe_s = {
    ('Quentin', 174),
    ('Ilyas' , 180),
    ('Emma' , 163)
}


#Pour les dictionnaires
def taille_moy_d(classe:Dict[str,int]) -> int :
    """
    Renvoie la moyenne des tailles d'une classe d'élèves modélisée par un dictionnaire nom:taille à l'entier inférireur près.
    >>>taille_moy({'Gilbert':171 , 'Anne':180})
    175
    """
    som_taille=0
    nb_eleves=0
    for taille in classe.values():
        som_taille += taille
        nb_eleves += 1
    return(som_taille//nb_eleves)

def moins_de_2(classe:Dict[str,int]) -> int :
    """
    Renvoie le nombre d'élèves qui ont une taille de moins de 2 cm de la taille moyenne
    >>>moins_de_2({'Gilbert':171 , 'Anne':180})
    1
    """
    eleves = 0
    moy = taille_moy_d(classe)
    for taille in classe.values():
        if taille <= moy + 2 and taille >= moy - 2:
            eleves = eleves + 1
    return(eleves)

def voyelles(classe:Dict[str,int]) -> int :
    """
    Renvoie le nombre délèves dont le nom commence par une voyelle
    >>>voyelles({'Gilbert':171 , 'Anne':180})
    """

def plus_que(classe: Dict[str,int], seuil: int) -> int :
    """
    Renvoie le nombre d'élèves de la classe dont la taille est supérieur au seuil
    >>>plus_que({'A':180,'B':190,'C'}:200},183)
    2
    """
    compteur=0
    for taille in classe.values():
        if taille > seuil :
            compteur = compteur + 1
    return(compteur)

#Pour les listes
def taille_moy_l(classe:List[Tuple[str,int]]) -> int :
    """
    Renvoie la moyenne des tailles d'une classe d'élèves modélisée par un dictionnaire nom:taille à l'entier inférireur près.
    >>>taille_moy[('Gilbert':171 , 'Anne':180)]
    175
    """
    som_taille=0
    nb_eleves=0
    for nom,taille in classe:
        som_taille += taille
        nb_eleves += 1
    return(som_taille//nb_eleves)

    def voyelles(classe:List[Tuple[str,int]]) -> int :
        """
        Renvoie le nombre délèves dont le nom commence par une voyelle
        >>>voyelles[('Gilbert':171 , 'Anne':180)]
        1
        """
        eleves = 0
        for nom in classe[0]:
            if nom(0).upper == "A" or "I" or "E" or "O" or "U" or "Y":
                eleves = eleves + 1 
        return(eleves)

        

#Pour les ensmebles
def taille_moy_s(classe:Set[Tuple[str,int]]) -> int :
    """
    Renvoie la moyenne des tailles d'une classe d'élèves modélisée par un dictionnaire nom:taille à l'entier inférireur près.
    >>>taille_moy{('Gilbert':171 , 'Anne':180)}
    175
    """
    som_taille=0
    nb_eleves=0
    for i in range(len(classe)):
        som_taille += classe(i)(2)
        nb_eleves += classe(i)(1)
    return(som_taille//nb_eleves)






###Le Programme
#L'entrée
padawans =  { 'Quentin' : 174,'Ilyas' : 180,'Emma' : 163 }
#L'application
moy = taille_moy_d(padawans)
petit = moins_de_2(padawans)
voy = voyelles(padawans)
#La sortie
print(voy)


